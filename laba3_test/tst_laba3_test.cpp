#include <QtTest>
#include "tst_laba3_test.h"
#include <../functions.h>

// add necessary includes here

laba3_test::laba3_test()
{

}

laba3_test::~laba3_test()
{

}

void laba3_test::test_linearEq()
{
    int a = 10;
    int b = 10;
    QCOMPARE(-1, linearEq(a, b));
}

void laba3_test::test_quadrEq()
{
    float* x = new float[2];
    x[0] = 1;
    x[1] = -4;
    int a = 1;
    int b = 3;
    int c = -4;
    float* y = quadrEq(a, b ,c);
    QCOMPARE(true, arrCheck(x, y));
}

void laba3_test::test_arrayMin()
{
    int* a = new int[4];
    a[0] = 7;
    a[1] = 1;
    a[2] = 10;
    a[3] = 3;
    QCOMPARE(1, arrayMin(a, 4));
}

void laba3_test::test_progressEl()
{
    int a1 = 1;
    int d = 1;
    int i = 4;
    QCOMPARE(4, progressEl(a1, d, i));
}

void laba3_test::test_amountAr()
{
    int a1 = 1;
    int d = 1;
    int i = 4;
    QCOMPARE(10, amountAr(a1, d, i));
}

void laba3_test::test_amountG()
{
    int b1 = 1;
    int q = 2;
    int i = 4;
    QCOMPARE(15, amountG(b1, q, i));
}

QTEST_APPLESS_MAIN(laba3_test)

