#ifndef TST_LABA3_TEST_H
#define TST_LABA3_TEST_H
#include <QObject>

#endif // TST_LABA3_TEST_H

class laba3_test : public QObject
{
    Q_OBJECT

public:
    laba3_test();
    ~laba3_test();

private slots:
    void test_linearEq();
    void test_quadrEq();
    void test_arrayMin();
    void test_progressEl();
    void test_amountAr();
    void test_amountG();
};
